package com.example.s528768.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class BookingDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);}

    public void popup(View v) {
        Toast.makeText(getApplicationContext(),"This will generate a popup asking the user to confirm if he/she wants to cancel the booking",Toast.LENGTH_LONG).show();
    }
    public void logout(View v) {
        Intent sub = new Intent(this, Calender.class);
        startActivity(sub);
    }
}


