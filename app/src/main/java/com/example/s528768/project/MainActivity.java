package com.example.s528768.project;

import android.content.Intent;
import android.icu.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void roomavailability(View v)
    {
        Intent one=new Intent(this, Calender.class);
        startActivity(one);
    }
    public void yourbookings(View v)
    {
        Intent one=new Intent(this, loginpage.class);
        startActivity(one);
    }
}
