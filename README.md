# README #

Project: Library Study Room Management System -ANDROID Application

Summary:
A Library Application for a University which can manage the study rooms booking. This application includes 
features like checking the availability of the rooms on a specific date and for a specific time. This can cut
down the manual effort of the library reception and can even make the student or any resource user plan their
study time productively.

Team Members:
Gopi Sai Krishna Lemati	S528746
Kranthi Kumar Parelly	S528756
Unnathi Vallapureddy	S528768
Yeshwnath Rao Rangineni	S528759











### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact